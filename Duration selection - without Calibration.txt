form Duration Choice
positive sec
comment Credits: Ben Barsties in cooperation with Dr. Y. Maryn (Sint-Jan, General Hospital, Bruges)
endform


sec = sec

n = numberOfSelected ("Sound")
for i to n
	sound'i'= selected ("Sound", i)
endfor

for i to n
	select sound'i'
	durationVowel = Get total duration
	durationVowelHalf = durationVowel/2
	beginOfVowelSelection = durationVowelHalf-sec/2
	endOfVowelSelection = durationVowelHalf+sec/2
	Extract part... beginOfVowelSelection endOfVowelSelection Rectangular 1 no
endfor