# ACOUSTIC VOICE QUALITY INDEX (AVQI)
#
# This is an almost complete re-write of the original script by Maryn and
# Cortals to calculate their "Acoustic Voice Quality Index". By making better
# use of the facilities available in later versions of Praat, this script takes
# considerably less time to finish the analysis, while maintaining comparable
# results with the original.
#
# From the documentation of the original script:
#
# > It is advocated to estimate someone's dysphonia severity in both
# > continuous speech (i.e., 'cs') and sustained vowel (i.e., 'sv')
# > (Maryn et al., 2010). This script therefore runs on these two types of
# > recordings, and it important to name these recordings 'cs' and 'sv',
# > respectively.
# >
# > This script automatically (a) searches, extracts and then concatenates
# > the voiced segments of the continuous speech recording to a new sound; (b)
# > concatenates the sustained vowel recording to the new sound, (c) determines
# > the Smoothed Cepstral Peak Prominence, the Shimmer Local, the Shimmer
# > Local dB, the LTAS-slope, the LTAS-tilt and the Harmonics-to-Noise Ratio of
# > the concatenated sound signal, (d) calculates the AVQI-score mostly based
# > on the method of Maryn et al. (2010), and draws the oscillogram, the
# > narrow-band spectrogram with LTAS and the power-cepstrogram with power-
# > cepstrum of the concatenated sound signal to allow further interpretation.
# >
# > For the AVQI to be reliable, it is imperative that the sound recordings
# > are made in an optimal data acquisition conditions.
#
# Author: Re-implemented by Jose Joaquin Atria,
#         based on original work by Maryn and Corthals
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.
#
# Caveat: Differences in the output between the official versions of the script
# and this one are to be expected. Where possible, these have been reduced to
# the minimum. However, because of implementation details outside of our
# immediate control, some of these differences still remain. The user should
# decide whether those differences outweigh the benefits of the current
# implementation.

#! ~~~ params
#! in:
#!   Stop Hann band frequency range: >
#!     The frequency range of a stop-filter to be used on both
#!     analysis sounds (the continuous speech and the sustained vowel)
#!   Maximum voiced frequency: >
#!     The maximum rate of zero-crossings
#!   Zero-crossing window length: >
#!     The maximum rate of zero-crossings
#! selection:
#!   in:
#!     sound: 2
#!   out:
#!     table: 1
#! ~~~
#!
#! The so-called AVQI sound is a concatenation of the voiced parts
#! of the continuous speech recording and (by default) the last 3
#! seconds of the sustained vowel (unless otherwised specified).
#!
#! The script stores the AVQI in a Table object, which is selected
#! upon completion of the script. The Table includes all the other
#! variables used for calculation of the AVQI.
#!
form Calculate Acoustic Voice Quality Index...
  integer left_Stop_Hann_band 0
  integer right_Stop_Hann_band 34
  positive Maximum_voiced_frequency 1500
  real Zero_crossing_window_length 0.0275
  comment v2.03
endform

benchmark = number(environment$("AVQI_BENCHMARK"))
benchmark = if benchmark == undefined then 0 else benchmark fi

stop_band_lo = left_Stop_Hann_band
stop_band_hi = right_Stop_Hann_band

# ----------------------------------------------------------------------------
# PART 0:
# HIGH-PASS FILTERING OF THE SOUND FILES.
# ----------------------------------------------------------------------------

@detectSounds()
cs_original = detectSounds.cs
sv_original = detectSounds.sv

stopwatch
selectObject: cs_original

@highPassFilter(stop_band_lo, stop_band_hi)
cs_filtered = highPassFilter.id

selectObject: sv_original

@highPassFilter(stop_band_lo, stop_band_hi)
sv_filtered = highPassFilter.id

time = stopwatch
if benchmark
  appendInfoLine: "Part 0: ", fixed$(time, 3)
endif

# ----------------------------------------------------------------------------
# PART 1:
# DETECTION, EXTRACTION AND CONCATENATION OF
# THE VOICED SEGMENTS IN THE RECORDING
# OF CONTINUOUS SPEECH.
# ----------------------------------------------------------------------------

stopwatch

selectObject: cs_filtered

sampling_rate = Get sampling frequency
sampling_period = Get sampling period

selectObject: cs_filtered

cs_silences = To TextGrid (silences):
  ... 50, 0.003, -25, 0.1, 0.1, "silence", "sounding"

selectObject: cs_filtered, cs_silences

Extract intervals where: 1, 0, "does not contain", "silence"
parts = numberOfSelected("Sound")
for i to parts
  part[i] = selected("Sound", i)
endfor

only_loud = Concatenate
Rename: "only_loud"

for i to parts
  removeObject: part[i]
endfor
removeObject: cs_filtered, cs_silences

global_power = Get power in air

selectObject: only_loud
signal_end          = Get end time
window_start        = Get start time
window_length       = 0.03
window_end          = window_start + window_length
voiceless_threshold = global_power * 0.3

total_windows = ceiling((signal_end - window_start) / window_length)

voiced_parts = 0
for w to total_windows
  voiced = 0
  window_start = (w - 1) * window_length
  window_end   = window_start + window_length
  window_end   = if window_end > signal_end then signal_end else window_end fi

  selectObject: only_loud

  part = Extract part: window_start, window_end, "Rectangular", 1, "no"
  partial_power = Get power in air

  if partial_power > voiceless_threshold
    @calculateZeroCrossing(zero_crossing_window_length)
    zc_rate = calculateZeroCrossing.rate

    if zc_rate != undefined and zc_rate < (maximum_voiced_frequency * 2)
      voiced = 1
    endif
  endif

  if voiced
    voiced_parts += 1
    voiced_part[voiced_parts] = part
  else
    removeObject: part
  endif
endfor

for i to voiced_parts
  plusObject: voiced_part[i]
endfor

only_voice = Concatenate
Rename: "only_voice"

nocheck selectObject: undefined
for i to voiced_parts
  removeObject: voiced_part[i]
endfor
removeObject: only_loud

#! ~~~ params
#! in:
#!   .end: The length of the analysis window
#! out:
#!   .rate: The zero-crossing rate within that window
#! selection:
#!   in:
#!     sound: 1
#! ~~~
#!
procedure calculateZeroCrossing (.end)
  .snd = selected("Sound")
  .rate = undefined

  .pp = To PointProcess (zeroes): 1, "yes", "yes"

  .points = Get number of points
  if .points
    .first = Get time from index: 1
    .last_index = Get high index: .end
    .last = Get time from index: .last_index
    .rate = (.last_index - 1) / (.last - .first)
  endif

  removeObject: .pp
  selectObject: .snd
endproc

time = stopwatch
if benchmark
  appendInfoLine: "Part 1: ", fixed$(time, 3)
endif

# ----------------------------------------------------------------------------
# PART 2:
# DETERMINATION OF THE SIX ACOUSTIC MEASURES
# AND CALCULATION OF THE ACOUSTIC VOICE QUALITY INDEX.
# ----------------------------------------------------------------------------

selectObject: sv_filtered

vowel_length = Get total duration
vowel_start = vowel_length - 3
if vowel_length > 3
  sv_part = Extract part: vowel_start, vowel_length, "Rectangular", 1, "no"
  Rename: "sv2"
else
  sv_part = Copy: "sv2"
endif

selectObject: only_voice
only_voice_length = Get total duration

plusObject: sv_part
avqi_sound = Concatenate
Rename: "avqi"
removeObject: sv_filtered, sv_part, only_voice

avqi_length = Get total duration
minimum_SPL = Get minimum: 0, 0, "None"
maximum_SPL = Get maximum: 0, 0, "None"

#!
#! ## Narrow-band spectrogram and LTAS
#!

stopwatch

selectObject: avqi_sound
ltas = To Ltas: 1
minimum_spectrum = Get minimum: 0, 4000, "None"
maximum_spectrum = Get maximum: 0, 4000, "None"

time = stopwatch
if benchmark
  appendInfoLine: "Part 2 - Spectrogram and LTAS: ", fixed$(time, 3)
endif

#!
#! ## Smoothed cepstral peak prominence (CPPS)
#!

stopwatch

selectObject: avqi_sound
cepstrogram = noprogress To PowerCepstrogram: 60, 0.002, 5000, 50
cpps = Get CPPS:
  ... "no", 0.01, 0.001,
  ... 60, 330, 0.05, "Parabolic",
  ... 0.001, 0, "Straight", "Robust"
removeObject: cepstrogram

time = stopwatch
if benchmark
  appendInfoLine: "Part 2 - Cepstra: ", fixed$(time, 3)
endif

#!
#! ## Slope of the long-term average spectrum
#!

stopwatch

selectObject: ltas
slope = Get slope: 0, 1000, 1000, 10000, "energy"

time = stopwatch
if benchmark
  appendInfoLine: "Part 2 - Slope: ", fixed$(time, 3)
endif

#!
#! ## Tilt of trendline through the long-term average spectrum
#!

stopwatch

trend = Compute trend line... 1 10000
tilt = Get slope: 0, 1000, 1000, 10000, "energy"
removeObject: ltas, trend

time = stopwatch
if benchmark
  appendInfoLine: "Part 2 - Tilt: ", fixed$(time, 3)
endif

#!
#! ## Amplitude perturbation measures
#!

stopwatch

selectObject: avqi_sound
pointprocess = noprogress To PointProcess (periodic, cc): 50, 400

selectObject: avqi_sound, pointprocess
shimmer = Get shimmer (local): 0, 0, 0.0001, 0.02, 1.3, 1.6
shimmer *= 100
shimmer_db = Get shimmer (local_dB): 0, 0, 0.0001, 0.02, 1.3, 1.6
removeObject: pointprocess

time = stopwatch
if benchmark
  appendInfoLine: "Part 2 - Perturbation measures: ", fixed$(time, 3)
endif

#!
#! ## Harmonic-to-noise ratio
#!

stopwatch

selectObject: avqi_sound
pitch_floor = 75
harmonicity = noprogress To Harmonicity (cc):
  ... 0.75 / pitch_floor, pitch_floor, 0.03, 1
hnr = Get mean: 0, 0
removeObject: avqi_sound, harmonicity

time = stopwatch
if benchmark
  appendInfoLine: "Part 2 - HNR: ", fixed$(time, 3)
endif

# Calculation of the AVQI

@calculateAVQI(cpps, hnr, shimmer, shimmer_db, slope, tilt)
avqi = calculateAVQI.avqi

avqi_table = Create Table with column names: "AVQI", 1,
  ... "CPPS HNR Shimmer Shimmer_dB Slope Tilt AVQI"
Set numeric value: 1, "CPPS",       cpps
Set numeric value: 1, "HNR",        hnr
Set numeric value: 1, "Shimmer",    shimmer
Set numeric value: 1, "Shimmer_dB", shimmer_db
Set numeric value: 1, "Slope",      slope
Set numeric value: 1, "Tilt",       tilt
Set numeric value: 1, "AVQI",       avqi

#! ~~~ params
#! in:
#!   .cpps: The cepstral peak prominence of the AVQI sound
#!   .hnr: The mean harmonic-to-noise ratio of the AVQI sound
#!   .shimmer: The local shimmer of the AVQI sound (as a percent)
#!   .shimmer_db: The local shimmer of the AVQI sound (in dB)
#!   .slope: The slope of the LTAS of the AVQI sound
#!   .tilt: Tilt of trendline through the LTAS of the AVQI sound
#! out:
#!   .avqi: The resulting AVQI
#! ~~~
#!
#! The so-called AVQI sound is a concatenation of the voiced parts
#! of the continuous speech recording and (by default) the last 3
#! seconds of the sustained vowel (unless otherwised specified).
#!
procedure calculateAVQI (.cpps, .hnr, .shimmer, .shimmer_db, .slope, .tilt)
  .avqi  = 3.295
  .avqi -= (0.111 * .cpps)
  .avqi -= (0.073 * .hnr)
  .avqi -= (0.213 * .shimmer)
  .avqi += (2.789 * .shimmer_db)
  .avqi -= (0.032 * .slope)
  .avqi += (0.077 * .tilt)
  .avqi *= 2.208
  .avqi += 1.797
endproc

#! ~~~ params
#! in:
#!   .lo: The low frequency boundary of the stop filter
#!   .hi: The high frequency boundary of the stop filter
#! out:
#!   .id: The ID of the filtered sound
#! selection:
#!   in:
#!     sound: 1
#! ~~~
#!
#! Apply a stop filter on the selected Sound object
#!
procedure highPassFilter (.lo, .hi)
  .snd = selected("Sound") ; Assert selection
  .id = Filter (stop Hann band): .lo, .hi, 0.1
endproc

#! ~~~ params
#! out:
#!   .cs: The ID of the Sound object with the continuous speech
#!   .sv: The ID of the Sound object with the sustained vowel
#! selection:
#!   in:
#!     sound: 2
#! internal: true
#! ~~~
#!
#! Attempt to automatically detect which of the two selected Sound
#! objects is the sustained vowel, and which is the continuous speech.
#!
#! Detection is made first based on object names, for backwards
#! compatibility, and then by comparing the objects duration.
#!
#! If both methods fail, the first selected object is silently assumed
#! to be the sustained vowel.
#!
procedure detectSounds ()
  if numberOfSelected("Sound") != 2
    exitScript: "Please select the sustained vowel and continuous speech recordings"
  endif
  .snd1  = selected("Sound", 1)
  .snd2  = selected("Sound", 2)

  .snd1$ = selected$("Sound", 1)
  .snd2$ = selected$("Sound", 2)

  # Detection by name
  if .snd1$ + .snd2$ == "cssv"
    .cs = .snd1
    .sv = .snd2
  elsif .snd1$ + .snd2$ == "svcs"
    .sv = .snd1
    .cs = .snd2
  # Detection by length
  else
    selectObject: .snd1
    .dur1 = Get total duration
    selectObject: .snd2
    .dur2 = Get total duration
    if .dur1 > .dur2
      .cs = .snd1
      .sv = .snd2
    # Default: assume sustained vowel comes first
    else
      .sv = .snd1
      .cs = .snd2
    endif
  endif

  selectObject: .cs, .sv
endproc
